#include <iostream>

using namespace std;

class Vector
{
private:
	double x;
	double y;
	double z;

public:
	Vector() : x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void print()
	{
		cout << "\nYour Vector : ( " << x << ", " << y << ", " << z << " )";
	}

	double lenght()
	{
		double a = 0;
		a = sqrt(x * x + y * y + z * z);
		return a;
	}
};

int main()
{
	cout << "Input X = ";
	double _x = 0;
	cin >> _x;

	cout << "Input Y = ";
	double _y = 0;
	cin >> _y;

	cout << "Input Z = ";
	double _z = 0;
	cin >> _z;

	Vector func(_x, _y, _z);
	func.print();
	cout << "\nVector Lenght : " << func.lenght() << '\n';

	return 0;
}